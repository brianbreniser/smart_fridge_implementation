package UseCases;

import Entities.FridgeItem;

import java.util.List;

public interface AbstractStorage {
    public void save(FridgeItem fridgeItem);
    public FridgeItem load(String id);
    public List<FridgeItem> getItemsBelowFillFactorThreshold(Double ff);
    public List<FridgeItem> getItemsOfCertainItemType(long itemType);
    public void delete(String id);
    public void dropDB();
}
