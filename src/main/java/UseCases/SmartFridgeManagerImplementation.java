package UseCases;

import Entities.FridgeItem;
import SmartFridgeInterface.SmartFridgeManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SmartFridgeManagerImplementation implements SmartFridgeManager {
    private AbstractStorage storage;
    private List<Long> ignoredItemTypes;

    public void resetIgnoredItems() {
        this.ignoredItemTypes.clear();
    }

    public SmartFridgeManagerImplementation(AbstractStorage storageImplementation) {
        this.storage = storageImplementation;
        this.ignoredItemTypes = new ArrayList<>();
    }

    @Override
    public void handleItemRemoved(String itemUUID) {
        this.storage.delete(itemUUID);
    }

    @Override
    public void handleItemAdded(long itemType, String itemUUID, String name, Double fillFactor) {
        FridgeItem fridgeItem = new FridgeItem(itemUUID, name, itemType, fillFactor);

        // Theoretically in either fridgeItem or here we would do some sort of validation
        // For now I can't think of anything the compiler won't catch, so save away!

        storage.save(fridgeItem);
    }

    @Override
    public Object[] getItems(Double fillFactor) {
        List<FridgeItem> items = this.storage.getItemsBelowFillFactorThreshold(fillFactor);

        // Combine the filtering out of ignored item types with the return statement, looks a bit cleaner to me
        return items.stream()
                .filter(x -> !this.ignoredItemTypes.contains(x.getItemType()))
                .toArray();
    }

    @Override
    public Double getFillFactor(long itemType) {

        if (this.ignoredItemTypes.contains(itemType)) {
            return 0.0;
        }

        List<FridgeItem> items = this.storage.getItemsOfCertainItemType(itemType);

        // we need the size, after filtering out the zero fill factor items
        Double size = (double) items.stream()
                .filter(x -> x.getFillFactor() != 0)
                .collect(Collectors.toList())
                .size();

        // we need the sum, after filtering out the zero fill factor items
        Double sum = items.stream()
                .map(FridgeItem::getFillFactor)
                .collect(Collectors.toList())
                .stream()
                .filter(x -> x != 0)
                .reduce((double) 0, (x, y) -> x + y);

        // If after filtering out the zero fill items, we have nothing, then we can return 0 as our average size
        if (sum == 0 || size == 0) {
            return 0.0;
        } else {
            return sum/size;
        }

    }

    @Override
    public void forgetItem(long itemType) {
        // I believe, from the interface explanation, that we don't want to remove items from the fridge in this case
        // We want items that are in the fridge now, or ever, to be ignored, with a particular item type

        this.ignoredItemTypes.add(itemType);
    }
}
