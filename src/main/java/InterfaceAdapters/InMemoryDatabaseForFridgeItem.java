package InterfaceAdapters;

import Entities.FridgeItem;
import UseCases.AbstractStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryDatabaseForFridgeItem implements AbstractStorage {
    private Map<String, FridgeItem> db;

    public InMemoryDatabaseForFridgeItem() {
        this.db = new HashMap<>();
    }

    @Override
    public void save(FridgeItem fridgeItem) {
        this.db.put(fridgeItem.getItemUuid(), fridgeItem);
    }

    @Override
    public FridgeItem load(String id) {
        return this.db.get(id);
    }

    @Override
    public List<FridgeItem> getItemsBelowFillFactorThreshold(Double ff) {
        // Woah boy this is not very effecient at scale
        // This is the quick implementation, if I have time I'll clean it up/make it faster
        // This is also how I would approach production code, write the thing that works, then make it better
        // All part of the red/green/refactor cycle of TDD

        List<FridgeItem> items = new ArrayList<>(this.db.values());

        // Filtered list based on if the fridge item has a lower fill factor than the Double that came in
        return items.stream()
                .filter(x -> x.isLowerThanFillFactor(ff))
                .collect(Collectors.toList());
    }

    @Override
    public List<FridgeItem> getItemsOfCertainItemType(long itemType) {
        // Woah boy this is not very effecient at scale
        // This is the quick implementation, if I have time I'll clean it up/make it faster
        // This is also how I would approach production code, write the thing that works, then make it better
        // All part of the red/green/refactor cycle of TDD

        List<FridgeItem> items = new ArrayList<>(this.db.values());

        // Filtered list based on if the fridge item has a lower fill factor than the Double that came in
        return items.stream()
                .filter(x -> x.isItemTypeOfThisType(itemType))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(String id) {
        this.db.remove(id);
    }

    @Override
    public void dropDB() {
        this.db.clear();
    }
}
