package Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FridgeItem {
     private String itemUuid;
     private String name;
     private Long itemType;
     private Double fillFactor;

     public boolean isLowerThanFillFactor(Double ff) {
          return this.fillFactor < ff;
     }

     public boolean isItemTypeOfThisType(Long itemType) {
         return this.itemType == itemType;
     }
}
