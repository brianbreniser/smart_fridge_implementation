package UseCases;

import Entities.FridgeItem;
import InterfaceAdapters.InMemoryDatabaseForFridgeItem;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SmartFridgeManagerImplementationTest {
    private SmartFridgeManagerImplementation fridgeManager;
    private InMemoryDatabaseForFridgeItem storage;

    @BeforeClass
    public void setUp() {
        // need a fresh db
        this.storage = new InMemoryDatabaseForFridgeItem();

        // need a fridge implemenation, loading up our db
        this.fridgeManager = new SmartFridgeManagerImplementation(storage);
    }

    @AfterMethod
    public void tearDown() {
        // keep a clean DB to work with in-between tests
        // this is technically a side effect, as the fridgeManager should "own" this db by now, but it's okay for integration tests
        this.storage.dropDB();
        this.fridgeManager.resetIgnoredItems();
    }

    @Test
    public void testHandleItemAdded() {
        fridgeManager.handleItemAdded(1, "uniqueid", "example fridge item", 1.5);
        FridgeItem fridgeItem = storage.load("uniqueid");

        assertEquals(fridgeItem.getName(), "example fridge item");
    }

    @Test
    public void testHandleItemRemoved() {
        fridgeManager.handleItemAdded(1, "removeMe", "example fridge item", 1.5);

        FridgeItem fridgeItem = storage.load("removeMe");
        assertEquals(fridgeItem.getName(), "example fridge item");

        fridgeManager.handleItemRemoved("removeMe");

        FridgeItem fridgeItem2 = storage.load("removeMe");
        assertNull(fridgeItem2);
    }

    @Test
    public void testGetItems() {
        // add a bunch of items with various fill factors

        // things that should be returned
        fridgeManager.handleItemAdded(1, "one", "dill pickles", 0.1);
        fridgeManager.handleItemAdded(1, "two", "sweet pickles", 0.2);
        fridgeManager.handleItemAdded(2, "three", "mayo", 0.3);

        // things that should not be returned
        fridgeManager.handleItemAdded(2, "four", "spicy mayo", 0.9);
        fridgeManager.handleItemAdded(3, "five", "2% milk", 1.0);
        fridgeManager.handleItemAdded(3, "six", "whole milk", 1.0);

        // get all items that have lower than fill factor 0.5 (I'm assuming 50% full)
        Object[] items = fridgeManager.getItems(0.5);

        // This Object[] here is frustrating, because I can't test the actual entities data
        // However, the creator of this interface probably didn't know I would try to use Entities
        // So for now, I think testing length is sufficient

        assertEquals(items.length, 3);
    }

    @Test
    public void testGetFillFactor() {
        fridgeManager.handleItemAdded(1, "one", "dill pickles", 0.1);
        fridgeManager.handleItemAdded(1, "two", "sweet pickles", 0.2);
        fridgeManager.handleItemAdded(2, "three", "mayo", 0.3);
        fridgeManager.handleItemAdded(2, "four", "spicy mayo", 0.9);
        fridgeManager.handleItemAdded(3, "five", "2% milk", 1.0);
        fridgeManager.handleItemAdded(3, "six", "whole milk", 1.0);

        // try to get the fill factor for item type 2
        assertEquals(fridgeManager.getFillFactor(2), (0.3+0.9)/2);
        assertEquals(fridgeManager.getFillFactor(1), (0.1+0.2)/2);
        assertEquals(fridgeManager.getFillFactor(3), 1.0);

        fridgeManager.forgetItem(3);
        assertEquals(fridgeManager.getFillFactor(3), 0.0);

        // No matter how many items we add with fill factor 0, so long as we have at least 1 item > 0, ignore the 0 items
        fridgeManager.handleItemAdded(2, "four1", "spicy mayo", 0.0);
        fridgeManager.handleItemAdded(2, "four2", "spicy mayo", 0.0);
        fridgeManager.handleItemAdded(2, "four3", "spicy mayo", 0.0);
        fridgeManager.handleItemAdded(2, "four4", "spicy mayo", 0.0);
        assertEquals(fridgeManager.getFillFactor(2), (0.3+0.9)/2);
    }

    @Test
    public void testForgetItem() {
        fridgeManager.handleItemAdded(1, "one", "dill pickles", 0.1);
        fridgeManager.handleItemAdded(1, "two", "sweet pickles", 0.2);
        fridgeManager.handleItemAdded(2, "three", "mayo", 0.3);
        fridgeManager.handleItemAdded(2, "four", "spicy mayo", 0.8);
        assertEquals(fridgeManager.getItems(0.9).length, 4);

        fridgeManager.forgetItem(1);
        assertEquals(fridgeManager.getItems(0.9).length, 2);

        fridgeManager.handleItemAdded(1, "another", "sweet pickles", 0.2);
        assertEquals(fridgeManager.getItems(0.9).length, 2);

        fridgeManager.handleItemAdded(2, "another2", "mayo", 0.3);
        assertEquals(fridgeManager.getItems(0.9).length, 3);
    }
}